package simulator.gates.combinational;

import simulator.network.Link;
import simulator.network.Node;

import java.util.List;

public class Explicit extends Node {
    public Explicit(String label, Boolean value) {
        super(label);
        addOutputLink(value);
    }

    @Override
    public final void evaluate() {
        //do nothing
    }

    @Override
    public List<Link> getInputs(int starIndex, int endIndex) {
        return null;
    }
}
