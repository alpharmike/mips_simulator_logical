package simulator.wrapper.wrappers;

import simulator.gates.combinational.Not;
import simulator.gates.combinational.Or;
import simulator.gates.combinational.Xor;
import simulator.network.Link;
import simulator.wrapper.Wrapper;

import java.util.ArrayList;

public class Comparator extends Wrapper {
    public Comparator(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        int dataSize = getInputSize() / 2;

        ArrayList<Xor> bit_xor = new ArrayList<>();

        for (int i = 0; i < dataSize; ++i) {
            bit_xor.add(new Xor(String.format("xor_%d", i), getInput(i), getInput(i + 32)));
        }

        Or or0 = new Or("or0");

        for (int i = 0; i < bit_xor.size(); ++i) {
            or0.addInput(bit_xor.get(i).getOutput(0));
        }

        Not output = new Not("not_output", or0.getOutput(0));

        addInput(output.getOutput(0));
    }
}
