package simulator.wrapper.wrappers;

import simulator.control.Simulator;
import simulator.network.Link;
import simulator.wrapper.Wrapper;

import java.util.ArrayList;
import java.util.List;

public class RightShifter extends Wrapper {

    public RightShifter(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        List<Link> input = getInputs(0, 32);
        List<Link> n = getInputs(32, 37);

        ArrayList<ArrayList<Link>> shifts = new ArrayList<>();

        for (int i = 0; i < 32; ++i) {
            shifts.add(new ArrayList<>());
            for (int j = 0; j < i; ++j) {
                shifts.get(i).add(Simulator.falseLogic);
            }
            shifts.get(i).addAll(input.subList(0, 32 - i));
        }


        ArrayList<Mux32x1> output = new ArrayList<>();

        for (int i = 0; i < shifts.size(); ++i) {
            Mux32x1 mux32x1 = new Mux32x1("mux32x1", "37X1");
            for (int j = 0; j < n.size(); ++j) {
                mux32x1.addInput(n.get(j));
            }

            for (int j = 0; j < shifts.size(); ++j) {
                mux32x1.addInput(shifts.get(j).get(i));
            }

            output.add(mux32x1);
        }

        for (int i = 0; i < output.size(); ++i) {
            addOutput(output.get(i).getOutput(0));
        }

    }
}
