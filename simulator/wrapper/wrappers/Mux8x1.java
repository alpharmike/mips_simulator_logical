package simulator.wrapper.wrappers;

import simulator.network.Link;
import simulator.wrapper.Wrapper;

public class Mux8x1 extends Wrapper {

    public Mux8x1(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        Link s2 = getInput(0);
        Link s1 = getInput(1);
        Link s0 = getInput(2);

        Mux4x1 mux4x1 = new Mux4x1("mux1", "6X1", s1, s0, getInput(3), getInput(4), getInput(5), getInput(6));
        Mux4x1 mux4x1_2 = new Mux4x1("mux2", "6X1", s1, s0, getInput(7), getInput(8), getInput(9), getInput(10));
        Mux2x1 mux2x1 = new Mux2x1("mux3", "3X1", s2, mux4x1.getOutput(0), mux4x1_2.getOutput(0));

        addOutput(mux2x1.getOutput(0));
    }
}
