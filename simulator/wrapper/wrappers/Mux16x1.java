package simulator.wrapper.wrappers;

import simulator.network.Link;
import simulator.wrapper.Wrapper;

public class Mux16x1 extends Wrapper {
    public Mux16x1(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        Link s3 = getInput(0);
        Link s2 = getInput(1);
        Link s1 = getInput(2);
        Link s0 = getInput(3);

        Mux8x1 mux8x1 = new Mux8x1("mux8x1", "11X1", s2, s1, s0);
        Mux8x1 mux8x1_2 = new Mux8x1("mux8x1_2", "11X1", s2, s1, s0);
        Mux2x1 mux2x1 = new Mux2x1("mux2x1", "3X1", s3);

        for (int i = 0; i < 8; ++i) {
            mux8x1.addInput(getInput(i + 4));
        }

        for (int i = 0; i < 8; ++i) {
            mux8x1_2.addInput(getInput(i + 12));
        }

        mux2x1.addInput(mux8x1.getOutput(0), mux8x1_2.getOutput(0));

        addOutput(mux2x1.getOutput(0));
    }
}
