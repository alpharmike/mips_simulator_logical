package simulator.wrapper.wrappers.nBit;

import simulator.gates.combinational.Xor;
import simulator.network.Link;
import simulator.wrapper.Wrapper;

import java.util.ArrayList;
import java.util.List;

public class NBitXor extends Wrapper {

    public NBitXor(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {

        List<Xor> xors = new ArrayList<>();

        int firstLowerBitIndex = (getInputSize() - 1) / 2;
        int secondLowerBitIndex = getInputSize() - 1;
        int fullOrSize = getInputSize() / 2;

        for (int i = 0; i < fullOrSize; ++i)
            xors.add(new Xor("Xor" + i, getInput(firstLowerBitIndex - i),
                    getInput(secondLowerBitIndex - i)));

        for (int i = fullOrSize - 1; i >= 0; --i)
            addOutput(xors.get(i).getOutput(0));

    }
}
