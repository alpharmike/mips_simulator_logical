package simulator.wrapper.wrappers.nBit;

import simulator.control.Simulator;
import simulator.gates.combinational.And;
import simulator.network.Link;
import simulator.wrapper.Wrapper;
import simulator.wrapper.wrappers.FullSubtractor;

import java.util.ArrayList;
import java.util.List;

public class NBitAnd extends Wrapper {

    public NBitAnd(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {

        List<And> ands = new ArrayList<>();

        int firstLowerBitIndex = (getInputSize() - 1) / 2;
        int secondLowerBitIndex = getInputSize() - 1;
        int fullAndSize = getInputSize() / 2;

        for (int i = 0; i < fullAndSize; ++i)
            ands.add(new And("And" + i, getInput(firstLowerBitIndex - i),
                    getInput(secondLowerBitIndex - i)));

        for (int i = fullAndSize - 1; i >= 0; --i)
            addOutput(ands.get(i).getOutput(0));
    }
}
