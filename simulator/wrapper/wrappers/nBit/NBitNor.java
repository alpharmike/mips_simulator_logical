package simulator.wrapper.wrappers.nBit;

import simulator.gates.combinational.And;
import simulator.gates.combinational.Nor;
import simulator.gates.combinational.Or;
import simulator.network.Link;
import simulator.wrapper.Wrapper;

import java.util.ArrayList;
import java.util.List;

public class NBitNor extends Wrapper {

    public NBitNor(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {

        List<Nor> nors = new ArrayList<>();

        int firstLowerBitIndex = (getInputSize() - 1) / 2;
        int secondLowerBitIndex = getInputSize() - 1;
        int fullOrSize = getInputSize() / 2;

        for (int i = 0; i < fullOrSize; ++i)
            nors.add(new Nor("Nor" + i, getInput(firstLowerBitIndex - i),
                    getInput(secondLowerBitIndex - i)));

        for (int i = fullOrSize - 1; i >= 0; --i)
            addOutput(nors.get(i).getOutput(0));

    }
}
