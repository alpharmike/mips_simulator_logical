package simulator.wrapper.wrappers.nBit;

import simulator.control.Simulator;
import simulator.network.Link;
import simulator.wrapper.Wrapper;
import simulator.wrapper.wrappers.Mux2x1;

public class SetOnLessThan extends Wrapper {
    public SetOnLessThan(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        //first input is the MSB of the subtractor, and the select signal for setting 1 or 0 for the output//

        Mux2x1[] mux2x1 = new Mux2x1[32];

        //initialize muxes and give them all their select signal//
        for(int i = 0; i < 32; i++)
            mux2x1[i] = new Mux2x1("mux2x1", "3X1", getInput(0));

        //pass 32 zeroes and 32 ones to the mux//
        for(int i = 0; i < 32; i++)
            mux2x1[i].addInput(Simulator.falseLogic);

        int i;

        for(i = 0; i < 31; i++)
            mux2x1[i].addInput(Simulator.falseLogic);

        mux2x1[i].addInput(Simulator.trueLogic);

        //put result on output signals//
        for(i = 0; i < 32; i++)
            addOutput(mux2x1[i].getOutput(0));

    }
}
