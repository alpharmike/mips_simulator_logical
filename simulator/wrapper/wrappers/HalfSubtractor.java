package simulator.wrapper.wrappers;

import simulator.gates.combinational.And;
import simulator.gates.combinational.Not;
import simulator.gates.combinational.Xor;
import simulator.network.Link;
import simulator.wrapper.Wrapper;

public class HalfSubtractor extends Wrapper {


    public HalfSubtractor(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        And and1 = new And("AND1");
        Xor xor1 = new Xor("XOR1");
        Not not1 = new Not("Not1");

        xor1.addInput(getInput(0), getInput(1));
        not1.addInput(getInput(0));
        and1.addInput(not1.getOutput(0), getInput(1));

        addOutput(xor1.getOutput(0), and1.getOutput(0));
        //output 0 : difference, output 1: borrow
    }
}
