package simulator.wrapper.wrappers;

import simulator.gates.combinational.Not;
import simulator.network.Link;
import simulator.wrapper.Wrapper;

import java.util.ArrayList;

public class Mux32x1 extends Wrapper {

    public Mux32x1(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        Link s4 = getInput(0);
        Link s3 = getInput(1);
        Link s2 = getInput(2);
        Link s1 = getInput(3);
        Link s0 = getInput(4);

        Mux16x1 mux16x1 = new Mux16x1("mux16x1", "20X1", s3, s2, s1, s0);
        Mux16x1 mux16x1_2 = new Mux16x1("mux16x1_2", "20X1", s3, s2, s1, s0);
        Mux2x1 mux2x1 = new Mux2x1("mux2x1", "3X1", s4);

        for (int i = 0; i < 16; ++i) {
            mux16x1.addInput(getInput(i + 5));
        }

        for (int i = 0; i < 16; ++i) {
            mux16x1_2.addInput(getInput(i + 21));
        }

        mux2x1.addInput(mux16x1.getOutput(0), mux16x1_2.getOutput(0));

        addOutput(mux2x1.getOutput(0));
    }
}
