package simulator.wrapper.wrappers;

import simulator.network.Link;
import simulator.wrapper.Wrapper;

public class SignExtend extends Wrapper {
    public SignExtend(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        Link sign = getInput(0);

        for (int i = 0; i < 16; ++i) {
            addOutput(sign);
        }

        for (int i = 0; i < 16; ++i) {
            addOutput(getInput(i));
        }
    }
}
