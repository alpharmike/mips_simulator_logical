package simulator.wrapper.wrappers;

import simulator.gates.combinational.And;
import simulator.gates.combinational.Not;
import simulator.gates.combinational.Or;
import simulator.network.Link;
import simulator.wrapper.Wrapper;

public class Mux4x1 extends Wrapper {
    public Mux4x1(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        Link s1 = getInput(0);
        Link s0 = getInput(1);

        Not not1 = new Not("N1", s1);
        Not not0 = new Not("N0", s0);

        And[] ands = new And[4];
        for (int i = 0; i < 4; ++i)
            ands[i] = new And("A" + i, getInput(i + 2));
        ands[0].addInput(not1.getOutput(0), not0.getOutput(0));
        ands[1].addInput(s0, not1.getOutput(0));
        ands[2].addInput(not0.getOutput(0), s1);
        ands[3].addInput(s0, s1);


        Or or = new Or("Or");
        for (int i = 0; i < 4; ++i)
            or.addInput(ands[i].getOutput(0));
            addOutput(or.getOutput(0));
    }
}
