package simulator.wrapper.wrappers;

import simulator.control.Simulator;
import simulator.network.Link;
import simulator.wrapper.Wrapper;

public class One extends Wrapper {
    public One(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        for (int i =0; i < 31; ++i) {
            addOutput(Simulator.falseLogic);
        }
        addOutput(Simulator.trueLogic);
    }
}
