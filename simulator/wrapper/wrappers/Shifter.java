package simulator.wrapper.wrappers;

import simulator.gates.combinational.Not;
import simulator.gates.combinational.Or;
import simulator.network.Link;
import simulator.wrapper.Wrapper;
import simulator.wrapper.wrappers.LeftShifter;
import simulator.wrapper.wrappers.Mux2x1;
import simulator.wrapper.wrappers.RightShifter;

public class Shifter extends Wrapper {
    public Shifter(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {

        // 32 5 3 : data shift amount aluControl

        LeftShifter leftShifter = new LeftShifter("LeftShifter", "37X32");
        RightShifter rightShifter = new RightShifter("RightShifter", "37X32");

        for (int i = 0; i < 37; i++) {
            leftShifter.addInput(getInput(i));
            rightShifter.addInput(getInput(i));
        }

        Mux2x1[] mux2x1s = new Mux2x1[32];

        //initialize muxes and give them the select signal//
        for (int i = 0; i < 32; i++) {
            mux2x1s[i] = new Mux2x1("mux2x1_" + i, "3X1", getInput(37));
            mux2x1s[i].addInput(leftShifter.getOutput(i), rightShifter.getOutput(i));
        }

        Or or0 = new Or("or0");
        for (int i = 0; i < 32; ++i) {
            or0.addInput(mux2x1s[i].getOutput(0));
        }

        Not notOr = new Not("notOr", or0.getOutput(0));

        addOutput(notOr.getOutput(0));
        for (int i = 0; i < 32; i++)
            addOutput(mux2x1s[i].getOutput(0));

    }
}