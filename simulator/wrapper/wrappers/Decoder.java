package simulator.wrapper.wrappers;

import com.mips_simulator.Utils.Padding;
import simulator.gates.combinational.And;
import simulator.gates.combinational.Not;
import simulator.network.Link;
import simulator.wrapper.Wrapper;

import java.util.ArrayList;

public class Decoder extends Wrapper {
    // the input size is one more than expected, for the enabled input
    public Decoder(String label, String stream, Link... links) {
        super(label, stream, links);
    }


    @Override
    public void initialize() {
        ArrayList<Link> inputs = new ArrayList<>();
        ArrayList<Not> notInputs = new ArrayList<>();

        Link enable = getInput(0);

        for (int i = 1; i < getInputSize(); ++i) {
            inputs.add(getInput(i));
            notInputs.add(new Not(String.format("not_%d", i), getInput(i)));
        }
        System.out.println(getInputSize());
        ArrayList<And> ands = new ArrayList<>();

        for (long j = 0; j < Math.pow(2, getInputSize() - 1); ++j) {
            String j_bin = Padding.padLeft(Long.toBinaryString(j), getInputSize() - 1);
            And and = new And(String.format("and_%d", j));
            for (int k = 0; k < getInputSize() - 1; ++k) {
                and.addInput(j_bin.charAt(k) == '1' ? inputs.get(k) : notInputs.get(k).getOutput(0));
            }

            and.addInput(enable);
            ands.add(and);

        }


        for (int i = 0; i < Math.pow(2, getInputSize() - 1); ++i) {
            addOutput(ands.get(i).getOutput(0));
        }
    }
}
