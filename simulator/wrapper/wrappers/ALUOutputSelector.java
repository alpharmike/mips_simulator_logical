package simulator.wrapper.wrappers;

import simulator.network.Link;
import simulator.wrapper.Wrapper;

import java.util.ArrayList;
import java.util.List;

public class ALUOutputSelector extends Wrapper {
    public ALUOutputSelector(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        // first bit is shift signal
        // next 33 bits is the output from alu
        // next 33 bits is the output from shifter

        Link signal = getInput(0);
        List<Link> data1 = getInputs(1, 34);
        List<Link> data2 = getInputs(34, 67);

        ArrayList<Mux2x1> bitSelectors = new ArrayList<>();

        for (int i = 0; i < 33; ++i) {
            Mux2x1 mux2x1 = new Mux2x1(String.format("mux_%d", i), "3X1", signal, data1.get(i), data2.get(i));
            bitSelectors.add(mux2x1);
        }

        for (int i = 0; i < 33; ++i) {
            addOutput(bitSelectors.get(i).getOutput(0));
        }
    }
}
