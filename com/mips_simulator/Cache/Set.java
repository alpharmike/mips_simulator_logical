package com.mips_simulator.Cache;

public class Set {
    private Boolean[] bits;
    private int way;

    public Set(int size) {
        this.bits = new Boolean[size];
        for (int i = 0; i < size; ++i) {
            this.bits[i] = false;
        }
        this.way = 0;
    }

    public Boolean[] getBits() {
        return bits;
    }

    public void setBits(Boolean[] bits) {
        this.bits = bits;
    }

    public int getWay() {
        return way;
    }

    public void setWay(int way) {
        this.way = way;
    }
}
