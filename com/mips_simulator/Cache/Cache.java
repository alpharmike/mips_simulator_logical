package com.mips_simulator.Cache;

import simulator.network.Link;
import simulator.network.Node;

import java.util.List;

/*
    a 2-way set associative cache
    20 bits used for tag
    7 bits used for set index
    5 bits as offset(line od address -> 32 bytes)
    each set contains: 2 * (20 + 32 * 8)  = 40(bits for tags) + 512(bits for line of data) + 2(bits to show validity) = 552 bits = 69 bytes
 */

public class Cache extends Node {
    private Set[] storage;

    public Cache(String label, Link... links) {
        super(label, links);
        storage = new Set[128];
        for (int i = 0; i < 128; ++i) {
            Set set = new Set(554);
            for (int j = 0; j < 554; ++j) {
                set.getBits()[j] = false;
            }
        }
        for (int i = 0; i < 33; ++i) {
            addOutputLink(false);
        }
    }

    private int address() {
        int temp = 0;
        for (int i = 1; i < 17; ++i) {
            if (inputs.size() > i) {
                if (getInput(i).getSignal()) {
                    temp += Math.pow(2, 16 - i);
                }
            }
        }

//        temp -= temp % 4;
        if (temp >= 65535 || temp <= 0)
            return 0;

//        temp++;
        return temp;
    }

    private int findSet() {
        List<Link> setIndex = getInputs(20, 27);
        int setIndexValue = 0;
        for (int i = 0; i < setIndex.size(); ++i) {
            if (setIndex.get(i).getSignal()) {
                setIndexValue += Math.pow(2, setIndex.size() - 1 - i);
            }
        }

        return setIndexValue;
    }

    private int getOffset() {
        List<Link> offset = getInputs(27, 32);
        int offsetValue = 0;
        for (int i = 0; i < offset.size(); ++i) {
            if (offset.get(i).getSignal()) {
                offsetValue += Math.pow(2, offset.size() - 1 - i);
            }
        }

        return offsetValue;
    }


    private void cacheWrite() {
        int setIndex = findSet();
        int offset = getOffset();
        int way = (storage[setIndex].getWay() == 0 && storage[setIndex].getBits()[0]) ? 1 : 0; // check way and validity
        storage[setIndex].setWay(way);
        if (offset < 8) {
            if (way == 0) {
                storage[setIndex].getBits()[0] = true;

                for (int i = 0; i < 32; ++i) {
                    if (i + offset * 32 + 21 <= 553) {
                        storage[setIndex].getBits()[i + 21 + offset * 32] = getInput(i + 32).getSignal();
                    } else {
                        storage[setIndex].getBits()[0] = false;
                    }
                }

                if (storage[setIndex].getBits()[0]) {
                    for (int i = 0; i < 20; ++i) {
                        storage[setIndex].getBits()[i + 1] = getInput(i).getSignal();
                    }
                }

            } else if (way == 1) {
                storage[setIndex].getBits()[277] = true;

                for (int i = 0; i < 32; ++i) {
                    if (i + 277 + 21 + offset * 32 <= 553)  {
                        storage[setIndex].getBits()[i + 277 + 21 + offset * 32] = getInput(i + 32).getSignal();
                    } else {
                        storage[setIndex].getBits()[277] = false;
                    }
                }

                if (storage[setIndex].getBits()[277]) {
                    for (int i = 0; i < 20; ++i) {
                        storage[setIndex].getBits()[i + 278] = getInput(i).getSignal();
                    }
                }

            }
        }


    }

    private boolean cacheRead() {
        Set set = storage[findSet()];
        boolean firstMatched, secondMatched;
        firstMatched = secondMatched = true;
        for (int i = 0; i < 20; ++i) {
            if (getInput(i).getSignal() != set.getBits()[i + 1]) {
                firstMatched = false;
            }

            if (getInput(i).getSignal() != set.getBits()[i + 278]) {
                secondMatched = false;
            }
        }

        int offset = getOffset();

        if (firstMatched) {
            getOutput(0).setSignal(true);
            for (int i = 0; i < 32; ++i) {
                if (i + offset * 32 + 21 <= 553) {
                    getOutput(i + 1).setSignal(set.getBits()[i + offset * 32 + 21]);
                }
            }
        } else if (secondMatched) {
            getOutput(0).setSignal(true);
            for (int i = 0; i < 32; ++i) {
                if (i + offset * 32 + 21 + 277 <= 553) {
                    getOutput(i + 1).setSignal(set.getBits()[i + offset * 32 + 277 + 21]);
                }
            }
        }

        return firstMatched || secondMatched;

    }

    @Override
    public void evaluate() {
        if (!cacheRead())
            cacheWrite();
    }

    public void setStorage(Set[] storage) {
        this.storage = storage;
    }

    public Set[] getStorage() {
        return storage;
    }

    @Override
    public List<Link> getInputs(int starIndex, int endIndex) {
        return null;
    }
}

