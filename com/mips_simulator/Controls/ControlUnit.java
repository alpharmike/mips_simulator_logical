package com.mips_simulator.Controls;

import simulator.gates.combinational.And;
import simulator.gates.combinational.Not;
import simulator.gates.combinational.Or;
import simulator.gates.sequential.ClockProvider;
import simulator.network.Link;
import simulator.wrapper.Wrapper;
import simulator.wrapper.wrappers.DFlipFlop;

import java.util.ArrayList;

public class ControlUnit extends Wrapper {
    public ControlUnit(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        Not notOp5 = new Not("not_op5", getInput(0));
        Not notOp4 = new Not("not_op4", getInput(1));
        Not notOp3 = new Not("not_op3", getInput(2));
        Not notOp2 = new Not("not_op2", getInput(3));
        Not notOp1 = new Not("not_op1", getInput(4));
        Not notOp0 = new Not("not_op0", getInput(5));

        And rFormat = new And("and1", notOp0.getOutput(0), notOp1.getOutput(0), notOp2.getOutput(0), notOp3.getOutput(0), notOp4.getOutput(0), notOp5.getOutput(0));
        And lw = new And("and2", getInput(0), notOp4.getOutput(0), notOp3.getOutput(0), notOp2.getOutput(0), getInput(4), getInput(5));
        And sw = new And("and3", getInput(0), notOp4.getOutput(0), getInput(2), notOp2.getOutput(0), getInput(4), getInput(5));
        And beq = new And("and4", notOp5.getOutput(0), notOp4.getOutput(0), notOp3.getOutput(0), getInput(3), notOp1.getOutput(0), notOp0.getOutput(0));
        And addi = new And("and5", notOp5.getOutput(0), notOp4.getOutput(0), getInput(2), notOp2.getOutput(0), notOp1.getOutput(0), notOp0.getOutput(0));

        Or aluSrc = new Or("or1", lw.getOutput(0), sw.getOutput(0), addi.getOutput(0));
        Or regWrite = new Or("or2", rFormat.getOutput(0), lw.getOutput(0), addi.getOutput(0));

        addOutput(
                rFormat.getOutput(0), // regDst
                aluSrc.getOutput(0), // aluSrc
                lw.getOutput(0), // memToReg
                regWrite.getOutput(0), // regWrite
                lw.getOutput(0), // memRead
                sw.getOutput(0), // memWrite
                beq.getOutput(0), // branch
                rFormat.getOutput(0), // aluOp1
                beq.getOutput(0) // aluOp0
        );

    }
}
