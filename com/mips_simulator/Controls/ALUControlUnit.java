package com.mips_simulator.Controls;

import simulator.gates.combinational.And;
import simulator.gates.combinational.Not;
import simulator.gates.combinational.Or;
import simulator.network.Link;
import simulator.wrapper.Wrapper;

public class ALUControlUnit extends Wrapper {

    public ALUControlUnit(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        Link aluOp1 = getInput(0);
        Link aluOp0 = getInput(1);

        Link funct5 = getInput(2);
        Link funct4 = getInput(3);
        Link funct3 = getInput(4);
        Link funct2 = getInput(5);
        Link funct1 = getInput(6);
        Link funct0 = getInput(7);

        Not notOp1 = new Not("not_op1", aluOp1);
        Not notOp0 = new Not("not_op0", aluOp0);
        Not notFunct2 = new Not("not_funct2", funct2);

        And and0 = new And("and0", aluOp1, funct1);
        Or or0 = new Or("or0", funct3, funct0);

        And op0 = new And("and_op0", aluOp1, or0.getOutput(0));
        Or op1 = new Or("or_op1", notOp1.getOutput(0), notFunct2.getOutput(0));
        Or op2 = new Or("or_op2", and0.getOutput(0), aluOp0);

        addOutput(
                op2.getOutput(0),
                op1.getOutput(0),
                op0.getOutput(0)
        );

    }
}
