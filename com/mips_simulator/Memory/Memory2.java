package com.mips_simulator.Memory;

import simulator.network.Link;
import simulator.network.Node;

import java.util.List;

public class Memory2 extends Node {
    private Byte2[] byte2s;

    // inputs 0-15 are address bits
    // inputs 16-47 are data bits
    // input 48 is MemWrite signal

    public Memory2(String label, Link... links) {
        super(label, links);
        this.byte2s = new Byte2[65536];
        for (int i = 0; i < 32; ++i) {
            this.addOutputLink(false);
        }
    }

    private int address() {
        int i, address = 0;
        for (i = 0; i < 16; ++i) {
            if (inputs.size() > i) {
                if (getInput(i).getSignal()) {
                    address += Math.pow(2, 15 - i);
                }
            }
        }
        return address;
    }

    private Byte2 byteInitialize(int address) {
        Byte2 b = new Byte2(label + "[" + Integer.toString(address) + "]", "8X8");
        int i;
        for (i = 0; i < 8; i++)
            b.addInput();

        return b;
    }

    private void memoryWrite() {
        int i, j, address = address();
        // Writing 4 bytes
        for (i = 0; i < 4; i++) {
            if (this.byte2s[address + i] == null) // This byte is not used before.
                this.byte2s[address + i] = byteInitialize(address + i);

            // Writing 8 bits on each byte
            for (j = 0; j < 8; j++) {
                Link memInput = this.getInput(i * 8 + j + 16);
                this.byte2s[address + i].getInput(j).setSignal(memInput.getSignal());
                this.setOutput(i * 8 + j, memInput);
            }
        }
    }

    private void memoryRead() {
        int i, j, address = address();
        // Reading 4 bytes
        for (i = 0; i < 4; i++) {
            if (this.byte2s[address + i] == null) // This byte is not used before.
                this.byte2s[address + i] = byteInitialize(address + i);

            // Reading 8 bits of each byte
            for (j = 0; j < 8; j++) {
                this.setOutput(i * 8 + j, this.byte2s[address + i].getOutput(j));
            }
        }
    }

    @Override
    public void evaluate() {

        if (this.getInput(48).getSignal()) {
            //System.out.println("Write");
            memoryWrite();
        } else {
            //System.out.println("Read");
            memoryRead();
        }
    }

    public Byte2[] getByte2s() {
        return byte2s;
    }

    public void setByte2s(Byte2[] byte2s) {
        this.byte2s = byte2s;
    }

    @Override
    public String toString() {
        String s = "";
        int i = 0;
        for (i = 0; i < 4; i++) {
            if (this.byte2s[i] != null)
                s += Integer.toString(i) + ") " + this.byte2s[i].toString() + "\n";
            else
                s += Integer.toString(i) + ") null\n";
        }
        return s;
    }

    @Override
    public List<Link> getInputs(int starIndex, int endIndex) {
        return null;
    }
}
