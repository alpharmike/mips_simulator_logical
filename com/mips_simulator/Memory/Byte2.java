package com.mips_simulator.Memory;

import simulator.network.Link;
import simulator.wrapper.Wrapper;

public class Byte2 extends Wrapper {

    private Boolean[] bits;

    public Byte2(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        int i;
        if (this.bits == null) {
            this.bits = new Boolean[8];
        }

        // big-endian
        for (i = 0; i < 8; i++) {
            this.bits[i] = this.getInput(i).getSignal();
            this.getOutput(i).setSignal(this.bits[i]);
        }
    }

    @Override
    public String toString() {
        int i;
        String s = "";
        for (i = 0; i < 8; i++){
            s += this.bits[i].toString() + " ";
        }
        return s;
    }
}
