package com.mips_simulator.Memory;

import java.util.Arrays;

public class Byte {
    private Boolean[] bits = {false, false, false, false, false, false, false, false};


    public Boolean[] getBits() {
        return this.bits;
    }

    public void setBits(Boolean[] bits) throws Exception {
        if (bits.length == this.bits.length)
            this.bits = bits;
        else
            throw new Exception("Size of a byte should be 8.");
    }

    public Boolean getBit(int index) {
        return this.bits[index];
    }

    public void setBit(int index, Boolean bit) {
        this.bits[index] = bit;
    }

    @Override
    public String toString() {
        return "NewerByte{" +
                "bits=" + Arrays.toString(bits) +
                '}';
    }
}
