package com.mips_simulator.Instruction;

public enum InstructionFormat {
    R_Type,
    I_Type,
    J_Type
}
